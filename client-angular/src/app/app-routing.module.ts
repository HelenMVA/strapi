import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./home-page/home-page.component";
import { MySpaceComponent } from "./my-space/my-space.component";
import { NavComponent } from './home-page/nav/nav.component';

const routes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "mySpace/:id", component: MySpaceComponent, canActivate: [NavComponent] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
