import { Component, OnInit } from "@angular/core";
import { AuthorService } from "src/app/services/author.service";
import { BooksService } from "src/app/services/books.service";

@Component({
  selector: "app-filter-author",
  templateUrl: "./filter-author.component.html",
  styleUrls: ["./filter-author.component.scss"],
})
export class FilterAuthorComponent implements OnInit {
  showDiv: boolean = true;
  authors;
  books: any;
  booksByAuthor: [];
  currentUser;
  currentAuthorName: string;
  constructor(
    private authorService: AuthorService,
    private bookService: BooksService
  ) {
    this.getAuthors();
  }

  ngOnInit() {
    this.getBooks();
    this.getCurrentUser();
  }
  getAuthors() {
    this.authorService.getAuthors().subscribe((res) => {
      this.authors = res;
      console.dir(this.authors);
    });
  }
  AllBooks() {
    this.booksByAuthor.splice(0, this.booksByAuthor.length);
    this.showDiv = false;
    console.dir(this.booksByAuthor);
    console.dir(this.showDiv);
  }
  getBookByAuthor(authorName) {
    this.booksByAuthor = this.books.filter(
      (book) => book.author.name === authorName
    );
    this.showDiv = true;
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  getBooks() {
    this.bookService.getBooks().subscribe((res) => {
      this.books = res;
    });
  }
  emprunter(bookId) {
    let currentUserId = this.currentUser.user.id;
    let body = {
      book: bookId,
      user: currentUserId,
    };
    this.bookService.borrowBook(body, this.currentUser);
  }
}
