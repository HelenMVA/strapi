import { Component, OnInit, SimpleChanges } from "@angular/core";
import { BooksService } from "src/app/services/books.service";
import { UserService } from "src/app/services/user.service";
import { AuthorService } from "src/app/services/author.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalDecriptionComponent } from "../modal-decription/modal-decription.component";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-card-book",
  templateUrl: "./card-book.component.html",
  styleUrls: ["./card-book.component.scss"],
})
export class CardBookComponent implements OnInit {
  books: any;
  sortedBooks;
  authors;
  currentUser;
  nbCurrentBook: any;
  nbNewBook: number;
  sortAsc: boolean = true;
  search;
  checkBorrowBook: number = 0;
  constructor(
    private bookService: BooksService,
    private userService: UserService,
    private authorService: AuthorService,
    private modalService: NgbModal,
    private toastService: ToastrService
  ) {}

  ngOnInit() {
    this.getBooks();
    this.getCurrentUser();
    this.getAuthors();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  getBooks() {
    this.bookService.getBooks().subscribe((res) => {
      this.books = res;
    });
  }
  emprunter(bookId) {
    let currentUserId = this.currentUser.user.id;

    let bodyBorrow = {
      idUser: bookId,
      idBook: currentUserId,
    };
    this.bookService.checkBookBorrowed(bodyBorrow).subscribe((res: any) => {
      // console.log(res.length);
      this.checkBorrowBook = res.length;
      if (this.checkBorrowBook == 0) {
        this.userService.getMy(currentUserId).subscribe((res: any) => {
          this.nbCurrentBook = res.nb_books_borrowed;
          if (this.nbCurrentBook < 5) {
            this.nbNewBook = this.nbCurrentBook += 1;
            this.currentUser.user.nb_books_borrowed = this.nbNewBook;
            let body = {
              book: bookId,
              user: currentUserId,
            };
            this.bookService.borrowBook(body, this.currentUser);
            this.updateUser(this.nbNewBook, currentUserId);
            let bodyBook = {
              available: false,
              id: bookId,
            };
            this.updateBook(bodyBook);
            this.toastService.success("Bonne lecture");
          } else {
            console.log("pas de droit");
          }
        });
      } else {
        console.log("livre deja emprunter");
        this.toastService.error("Vous avez deja cette livre");
      }
    });
  }
  updateUser(nbNewBook, currentUserId) {
    let body = {
      id: currentUserId,
      nb_books_borrowed: nbNewBook,
    };
    this.userService.updateUser(body).subscribe((res) => {
      this.nbCurrentBook = res;
    });
  }
  updateBook(body) {
    this.bookService.updateBook(body).subscribe((res) => {
      console.dir(res);
      this.getBooks();
    });
  }
  getAuthors() {
    this.authorService.getAuthors().subscribe((res) => {
      this.authors = res;
      console.dir(this.authors);
    });
  }
  getBookByAuthor(authorName) {
    this.authorService.getBookByAuthor(authorName).subscribe((res) => {
      this.sortedBooks = res;
      console.dir(this.sortedBooks);
    });
  }
  AllBooks() {
    this.sortedBooks.splice(0, this.sortedBooks.length);
  }

  openModalMoreInfo(book) {
    console.dir(book);
    const modalRef = this.modalService.open(ModalDecriptionComponent);
    modalRef.componentInstance.book = book;
  }

  modelChanged(e) {
    this.search = e.target.value;
    this.bookService.getBySearch(this.search).subscribe((res) => {
      this.sortedBooks = res;
    });
  }
  sortByAsc() {
    this.bookService.sortByAsc().subscribe((res) => {
      this.sortAsc = false;
      console.log(res);
      this.sortedBooks = res;
      console.dir(this.sortedBooks);
    });
  }
  sortByDesc() {
    this.bookService.sortByDesc().subscribe((res) => {
      this.sortAsc = true;
      console.log(res);
      this.sortedBooks = res;
      console.dir(this.sortedBooks);
    });
  }
}
