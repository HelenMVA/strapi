import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-decription",
  templateUrl: "./modal-decription.component.html",
  styleUrls: ["./modal-decription.component.scss"],
})
export class ModalDecriptionComponent implements OnInit {
  @Input() public book;
  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
    console.dir(this.book);
  }

  closeModal() {
    this.activeModal.close();
  }
}
