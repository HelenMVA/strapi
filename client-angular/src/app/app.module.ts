import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomePageComponent } from "./home-page/home-page.component";
import { HttpClientModule } from "@angular/common/http";
import { NavComponent } from "./home-page/nav/nav.component";
import { CardBookComponent } from "./home-page/card-book/card-book.component";
import { ConnectModalComponent } from "./home-page/connect-modal/connect-modal.component";
import { NgbModule, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";
import { FilterAuthorComponent } from "./home-page/filter-author/filter-author.component";
import { MySpaceComponent } from "./my-space/my-space.component";
import { ToastrModule } from "ngx-toastr";
import { ModalDecriptionComponent } from "./home-page/modal-decription/modal-decription.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavComponent,
    CardBookComponent,
    ConnectModalComponent,
    FilterAuthorComponent,
    MySpaceComponent,
    ModalDecriptionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
  ],
  providers: [NgbActiveModal, NavComponent],
  bootstrap: [AppComponent],
  entryComponents: [ConnectModalComponent, ModalDecriptionComponent],
})
export class AppModule {}
