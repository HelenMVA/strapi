import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class BooksService {
  curUser;
  apiURL = "http://localhost:1337/";

  constructor(private http: HttpClient) {}

  getBooks() {
    return this.http.get(`${this.apiURL}books`);
  }
  doLogin(currentUser) {
    this.curUser = JSON.parse(localStorage.getItem("currentUser"));
    console.dir(this.curUser.jwt);
  }

  borrowBook(body, currentUser) {
    this.doLogin(currentUser);
    var reqHeader = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.curUser.jwt,
    });

    const httpOptions = {
      headers: reqHeader,
    };
    // console.dir(body);
    return this.http
      .post(`${this.apiURL}reader-borrows-books`, body, httpOptions)
      .subscribe((res) => {
        console.dir(res);
      });
  }

  getMyBooks(id) {
    return this.http.get(`${this.apiURL}reader-borrows-books?user.id=${id}`);
  }
  deleteMyBook(id) {
    return this.http.delete(`${this.apiURL}reader-borrows-books/${id}`);
  }
  sortMyBookAsc(id) {
    console.log(id);
    return this.http.get(
      `${this.apiURL}reader-borrows-books?user.id=${id}&_sort=created_at:ASC`
    );
  }
  sortMyBookDesc(id) {
    console.log(id);
    return this.http.get(
      `${this.apiURL}reader-borrows-books?user.id=${id}&_sort=created_at:DESC`
    );
  }

  checkBookBorrowed(body) {
    return this.http.get(
      `${this.apiURL}reader-borrows-books?user.id=${body.idBook}&book.id=${body.idUser}`
    );
  }

  getBySearch(value) {
    return this.http.get(`${this.apiURL}books?name_containss=${value}`);
  }
  sortByAsc() {
    return this.http.get(`${this.apiURL}books?_sort=name:ASC`);
  }
  sortByDesc() {
    return this.http.get(`${this.apiURL}books?_sort=name:DESC`);
  }

  updateBook(body) {
    return this.http.put(`${this.apiURL}books/${body.id}`, body);
  }
}
